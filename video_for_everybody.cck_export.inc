<?php

/**
 * @file ccknodedef.inc
 * This file contains the exported code that
 * is used to create the video for everybody
 * content type performed on install.
 *
 * @see video_for_everybody_install().
 */
  
function _video_for_everybody_cck_export() {
  $content['type']  = array (
    'name' => 'Video for everybody',
    'type' => 'video_for_everybody',
    'description' => 'A video for everybody video. This content type allows you to upload a video in mp4 and ogv formats (along with an optional poster) which is output in the video for everybody format.',
    'title_label' => 'Title',
    'body_label' => 'Body',
    'min_word_count' => 0,
    'help' => '',
    'node_options' => 
    array (
      'status' => true,
      'promote' => true,
      'sticky' => false,
      'revision' => false,
    ),
    'old_type' => 'video_for_everybody',
    'orig_type' => 'video_for_everybody',
    'module' => 'video_for_everybody',
    'custom' => false,
    'modified' => false,
    'locked' => true,
    'reset' => 'Reset to defaults',
    'comment' => 2,
    'comment_default_mode' => 4,
    'comment_default_order' => 1,
    'comment_default_per_page' => 50,
    'comment_controls' => 3,
    'comment_anonymous' => 0,
    'comment_subject_field' => 1,
    'comment_preview' => 1,
    'comment_form_location' => 0,
  );
  $content['fields']  = array (
    0 => 
    array (
      'label' => 'MP4 Video',
      'field_name' => 'field_video_for_everybody_mp4',
      'type' => 'filefield',
      'widget_type' => 'filefield_widget',
      'change' => 'Change basic information',
      'weight' => '-4',
      'file_extensions' => 'mp4',
      'progress_indicator' => 'bar',
      'file_path' => 'v4e/mp4',
      'max_filesize_per_file' => '',
      'max_filesize_per_node' => '',
      'description' => '',
      'required' => 1,
      'multiple' => '0',
      'list_field' => '0',
      'list_default' => 1,
      'description_field' => '0',
      'op' => 'Save field settings',
      'module' => 'filefield',
      'widget_module' => 'filefield',
      'columns' => 
      array (
        'fid' => 
        array (
          'type' => 'int',
          'not null' => false,
          'views' => true,
        ),
        'list' => 
        array (
          'type' => 'int',
          'size' => 'tiny',
          'not null' => false,
          'views' => true,
        ),
        'data' => 
        array (
          'type' => 'text',
          'serialize' => true,
          'views' => true,
        ),
      ),
      'display_settings' => 
      array (
        'weight' => '-4',
        'parent' => '',
        4 => 
        array (
          'format' => 'hidden',
          'exclude' => 0,
        ),
        'label' => 
        array (
          'format' => 'hidden',
        ),
        'teaser' => 
        array (
          'format' => 'hidden',
          'exclude' => 0,
        ),
        'full' => 
        array (
          'format' => 'hidden',
          'exclude' => 0,
        ),
      ),
    ),
    1 => 
    array (
      'label' => 'OGV Video',
      'field_name' => 'field_video_for_everybody_ogv',
      'type' => 'filefield',
      'widget_type' => 'filefield_widget',
      'change' => 'Change basic information',
      'weight' => '-3',
      'file_extensions' => 'ogv',
      'progress_indicator' => 'bar',
      'file_path' => 'v4e/ogv',
      'max_filesize_per_file' => '',
      'max_filesize_per_node' => '',
      'description' => '',
      'required' => 1,
      'multiple' => '0',
      'list_field' => '0',
      'list_default' => 1,
      'description_field' => '0',
      'op' => 'Save field settings',
      'module' => 'filefield',
      'widget_module' => 'filefield',
      'columns' => 
      array (
        'fid' => 
        array (
          'type' => 'int',
          'not null' => false,
          'views' => true,
        ),
        'list' => 
        array (
          'type' => 'int',
          'size' => 'tiny',
          'not null' => false,
          'views' => true,
        ),
        'data' => 
        array (
          'type' => 'text',
          'serialize' => true,
          'views' => true,
        ),
      ),
      'display_settings' => 
      array (
        'weight' => '-3',
        'parent' => '',
        4 => 
        array (
          'format' => 'hidden',
          'exclude' => 0,
        ),
        'label' => 
        array (
          'format' => 'hidden',
        ),
        'teaser' => 
        array (
          'format' => 'hidden',
          'exclude' => 0,
        ),
        'full' => 
        array (
          'format' => 'hidden',
          'exclude' => 0,
        ),
      ),
    ),
    2 => 
    array (
      'label' => 'Poster Image',
      'field_name' => 'field_video_for_everybody_poster',
      'type' => 'filefield',
      'widget_type' => 'imagefield_widget',
      'change' => 'Change basic information',
      'weight' => '-2',
      'file_extensions' => 'png gif jpg jpeg',
      'progress_indicator' => 'bar',
      'file_path' => 'v4e/poster',
      'max_filesize_per_file' => '',
      'max_filesize_per_node' => '',
      'max_resolution' => 0,
      'min_resolution' => 0,
      'custom_alt' => 1,
      'alt' => '',
      'custom_title' => 0,
      'title_type' => 'textfield',
      'title' => '',
      'use_default_image' => 0,
      'default_image_upload' => '',
      'default_image' => NULL,
      'description' => '',
      'required' => 0,
      'multiple' => '0',
      'list_field' => '0',
      'list_default' => 1,
      'description_field' => '0',
      'op' => 'Save field settings',
      'module' => 'filefield',
      'widget_module' => 'imagefield',
      'columns' => 
      array (
        'fid' => 
        array (
          'type' => 'int',
          'not null' => false,
          'views' => true,
        ),
        'list' => 
        array (
          'type' => 'int',
          'size' => 'tiny',
          'not null' => false,
          'views' => true,
        ),
        'data' => 
        array (
          'type' => 'text',
          'serialize' => true,
          'views' => true,
        ),
      ),
      'display_settings' => 
      array (
        'weight' => '-2',
        'parent' => '',
        4 => 
        array (
          'format' => 'hidden',
          'exclude' => 0,
        ),
        'label' => 
        array (
          'format' => 'hidden',
        ),
        'teaser' => 
        array (
          'format' => 'hidden',
          'exclude' => 0,
        ),
        'full' => 
        array (
          'format' => 'hidden',
          'exclude' => 0,
        ),
      ),
    ),
  );
  $content['extra']  = array (
    'title' => '-5',
    'revision_information' => '0',
    'comment_settings' => '1',
    'menu' => '-1',
  );

  return $content;
}
