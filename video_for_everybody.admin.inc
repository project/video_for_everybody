<?php

/**
 * @file
 * The Video for everybody! module admin include which provides the settings form
 */

/**
 * Form builder. Configuration settings for our module
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function video_for_everybody_admin_settings() {
  $form = array();

  // player default settings
  $form['defaults'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  $form['defaults']['video_for_everybody_show_download_links'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show download video file links'),
    '#default_value' =>  variable_get('video_for_everybody_show_download_links', 0),
    '#description' => t('Output links to download the video files below the video player.'),
  );
  
  // unsure of htmls support for this sort of thing at the moment.
  /*$form['defaults']['video_autoplay'] = array(
    '#type' => 'checkbox',
    '#title' => t('Video should autoplay on page load'),
    '#default_value' =>  !empty($widget['video_autoplay']) ? $widget['video_autoplay'] : 0,
  );*/
  
  $form['defaults']['video_for_everybody_default_player_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Default player width'),
    '#default_value' => variable_get('video_for_everybody_default_player_width', 640),
    '#size' => 15,
    '#maxlength' => 4,
    '#description' =>
    t('The default width the video player should be.'),
  );
  
  $form['defaults']['video_for_everybody_default_player_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Default player height'),
    '#default_value' => variable_get('video_for_everybody_default_player_height', 360),
    '#size' => 15,
    '#maxlength' => 4,
    '#description' =>
    t('The default height the video player should be.'),
  );
  

  // Flash player selection
  $form['flash_players'] = array(
    '#type' => 'fieldset',
    '#title' => t('Flash Player settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  
  $available_players = _video_for_everbody_get_available_players();
  // Output a select field if the user has any players available
  if (!empty($available_players)) {
    $player_options = array();
    foreach ($available_players as $k => $player) {
      $player_options[$k] = $player['title'];
    }
    
    $form['flash_players']['video_for_everybody_flash_player'] = array(
      '#type' => 'select',
      '#title' => t('Flash Player'),
      '#default_value' => variable_get('video_for_everybody_flash_player', ''),
      '#options' => $player_options,
    );
  }
  else {
    // no players, output details on how/where to get some
    $form['flash_players']['where_to_find'] = array(
      '#type' => 'markup',
      '#value' => t('<p>In order to provide a fallback flash video player you must select and download an appropriate player. Please find below a list of supported players.</p>') .'
      <table>
        <thead><tr><th>'. t('Player') .'</th><th>'. t('Download Url') .'</th><th>'. t('Install location') .'</th></tr></thead>
        <tbody>
          <tr>
            <td>JW Player</td>
            <td><a href="http://www.jeroenwijering.com/?item=JW_FLV_Media_Player">http://www.jeroenwijering.com/?item=JW_FLV_Media_Player</a></td>
            <td><code>modules/video_for_everybody/assets/jw-player/4/player.swf</code></td>
          </tr>
        </tbody>
      </table>',
      /*
        <tr>
          <td>FlowPlayer 2</td>
          <td><a href="http://www.tucows.com/download.html?software_id=519713&t=2">http://www.tucows.com/download.html?software_id=519713&t=2</a></td>
          <td><code>modules/video_for_everybody/assets/flowplayer/2</code></td>
        </tr>
        <tr>
          <td>FlowPlayer 3</td>
          <td><a href="http://www.flowplayer.org/">http://www.flowplayer.org/</a></td>
          <td><code>modules/video_for_everybody/assets/flowplayer/3</code></td>
        </tr>*/
    );
  }

  return system_settings_form($form);
}
