
-- SUMMARY --

This module implements a new content type "Video for everybody" which has
three fields by default: MP4 video, OGV video and poster. This module at
the moment is quite basic and in early dev so use on production sites is
not recommended! 

A few things about this module:

1) It will not convert videos for you, you must upload your videos in MP4 
and OGV formats. You can download a free video converter here: 
http://www.mirovideoconverter.com/ (note: the page hints towards Mac 
only but there is a link to a windows version in the page footer).

2) Due to licensing issues you must download a flash player yourself, for
more information see the installation part of this document.
 

For a full description of the module, visit the project page:
  http://drupal.org/project/video_for_everybody

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/video_for_everybody

-- INSTALLATION --

* Install as usual, unzip the archive into your sites/all/modules
  directory

* Install a fallback flash video player which can play MP4 files,
  currently on JW Player 4 is supported, but this will expand. Go
  here: http://www.longtailvideo.com/players/jw-flv-player/ 
  Once downloaded simply place the 'player.swf' file in the
  following location within the video for everybody module:
  video_for_everybody/assets/jw-player/4/player.swf

* Enable the module

* Go to Administer >> Site Configuration >> Video for Everybody! Settings.
  Set the player width & height, and make sure 'Flash Player' has 'JW Player 4'
  selected, then click save.
  

-- USAGE --

* Currently the module is somewhat limited but this should be remedied
  once we reach a stable release point. Currently I recommend that you
  make use of Node Reference to allow association of videos to other
  nodes. More on this soon!


-- CONTACT --

Current maintainers:
* Alli Price (alli.price) - http://drupal.org/user/431193

      
      